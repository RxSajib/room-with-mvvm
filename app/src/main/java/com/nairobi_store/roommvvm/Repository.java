package com.nairobi_store.roommvvm;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class Repository {

    private NoteDio noteDio;
    private LiveData<List<Note>> allnote;


    public Repository(Application application){
        NoteDatabase noteDatabase = NoteDatabase.getInstance(application);
        noteDio = noteDatabase.noteDio();
        allnote = noteDio.getallnote();

    }

    public void insert(Note note){
        new InsertNoteAsyTask(noteDio).execute(note);
    }

    public void update(Note note){
        new UpdateNoteAsyTask(noteDio).execute(note);
    }

    public void delete(Note note){
        new DeleteNoteAsyTask(noteDio).execute(note);
    }

    public void deleteallnote(){
        new DeleteAllNoteAsyTask(noteDio).execute();
    }

    public LiveData<List<Note>> getAllnote() {
        return allnote;
    }



    private static class InsertNoteAsyTask extends AsyncTask<Note, Void , Void>{

        private NoteDio noteDio;

        public InsertNoteAsyTask(NoteDio noteDio) {
            this.noteDio = noteDio;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDio.insert(notes[0]);
            return null;
        }
    }

    private static class UpdateNoteAsyTask extends AsyncTask<Note, Void, Void>{

        private NoteDio noteDio;

        public UpdateNoteAsyTask(NoteDio noteDio) {
            this.noteDio = noteDio;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDio.update(notes[0]);
            return null;
        }
    }

    private static class DeleteNoteAsyTask extends AsyncTask<Note, Void, Void>{

        private NoteDio noteDio;

        public DeleteNoteAsyTask(NoteDio noteDio) {
            this.noteDio = noteDio;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDio.delete(notes[0]);
            return null;
        }
    }

    private static class DeleteAllNoteAsyTask extends AsyncTask<Void, Void, Void>{

        private NoteDio noteDio;

        public DeleteAllNoteAsyTask(NoteDio noteDio) {
            this.noteDio = noteDio;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDio.deleteallnote();
            return null;
        }
    }
}
