package com.nairobi_store.roommvvm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ViewModel extends AndroidViewModel {

    private Repository repository;
    private LiveData<List<Note>> allnote;


    public ViewModel(@NonNull Application application) {
        super(application);

        repository = new Repository(application);
        allnote = repository.getAllnote();
    }

    public void inset(Note note){
        repository.insert(note);
    }

    public void update(Note note){
        repository.update(note);
    }

    public void delete(Note note){
        repository.delete(note);
    }

    public void deleteallnote(){
        repository.deleteallnote();
    }

    public LiveData<List<Note>> getAllnote() {
        return allnote;
    }
}
