package com.nairobi_store.roommvvm;

import android.icu.text.CaseMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ListHolder>{

    private ArrayList<Note> noteArrayList = new ArrayList<>();

    @NonNull
    @Override
    public ListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_iteam, parent, false);
        return new ListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListHolder holder, int position) {
        Note note = noteArrayList.get(position);
        holder.title.setText(note.getTitle());
        holder.details.setText(note.getDetails());
        holder.sort.setText(String.valueOf(note.getPriority()));


    }

    @Override
    public int getItemCount() {
        return noteArrayList.size();
    }

    public void setiteam(List<Note> notes){
        noteArrayList = (ArrayList<Note>) notes;
        notifyDataSetChanged();
    }

    public class ListHolder extends RecyclerView.ViewHolder{

        private TextView title, details, sort;

        public ListHolder(@NonNull View itemView) {
            super(itemView);


            title = itemView.findViewById(R.id.TitleID);
            details  = itemView.findViewById(R.id.DetailsID);
            sort = itemView.findViewById(R.id.ShortTextID);
        }
    }
}
