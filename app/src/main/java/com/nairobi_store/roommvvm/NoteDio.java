package com.nairobi_store.roommvvm;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NoteDio {

    @Insert
    public void insert(Note note);


    @Update
    public void update(Note note);

    @Delete
    public void delete(Note note);


    @Query("DELETE  FROM note_table ")
    public void deleteallnote();


    @Query("SELECT * FROM note_table ORDER BY priority DESC")
    LiveData<List<Note>> getallnote();
}
