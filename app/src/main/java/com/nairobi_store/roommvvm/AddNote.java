package com.nairobi_store.roommvvm;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.security.SecureRandom;

public class AddNote extends AppCompatActivity {

    public static final String ActionTitle = "ActionTitle";
    public static final String ActionDetails = "ActionDetails";
    public static final String ActionPrority = "Prority";

    private EditText Title, Details;
    private NumberPicker numberPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_note);


        Title = findViewById(R.id.TitleInputID);
        Details = findViewById(R.id.DetailsInputID);
        numberPicker = findViewById(R.id.NumberPickerID);

        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(10);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.SaveMenuID){
            savedata();
        }
        return true;
    }


    private void savedata(){
        String Titletext = Title.getText().toString().trim();
        String Detailstext = Details.getText().toString().trim();
        int prority = numberPicker.getValue();

        if(Titletext.isEmpty()){
            Toast.makeText(this, "Title require", Toast.LENGTH_SHORT).show();
        }
        else if(Detailstext.isEmpty()){
            Toast.makeText(this, "Details require", Toast.LENGTH_SHORT).show();
        }


        else {
            Intent intent = new Intent();
            intent.putExtra(ActionTitle, Titletext);
            intent.putExtra(ActionDetails, Detailstext);
            intent.putExtra(ActionPrority, prority);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}