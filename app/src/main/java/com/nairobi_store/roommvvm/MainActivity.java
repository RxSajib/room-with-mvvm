package com.nairobi_store.roommvvm;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.EventListener;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ViewModel viewModel;
    private Adapter adapter;
    private RecyclerView recyclerView;
    private FloatingActionButton Addbutton;
    private int ButtonCode = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.RecylerViewID);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setHasFixedSize(true);

        adapter = new Adapter();
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this).get(ViewModel.class);
        viewModel.getAllnote().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {
                adapter.setiteam(notes);



            }
        });


        Addbutton = findViewById(R.id.AddButtonID);
        Addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddNote.class);
                startActivityForResult(intent, ButtonCode);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ButtonCode && resultCode == RESULT_OK){
            String title = data.getStringExtra(AddNote.ActionTitle);
            String details   = data.getStringExtra(AddNote.ActionDetails);
            int prority = data.getIntExtra(AddNote.ActionPrority, 1);


            Note note = new Note(title, details, prority);
            viewModel.inset(note);

        }
        else {

        }
    }
}