package com.nairobi_store.roommvvm;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {Note.class}, version = 1)
public abstract class NoteDatabase extends RoomDatabase {

    private static  NoteDatabase instant;


    public abstract NoteDio noteDio();

    public static synchronized NoteDatabase getInstance(Context context){
        if(instant == null){
            instant = Room.databaseBuilder(context.getApplicationContext(), NoteDatabase.class, "note_table")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomcallbacn)
                    .build();
        }

        return instant;
    }


    private static RoomDatabase.Callback roomcallbacn = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {

            super.onCreate(db);
            new PopulateBdAsyTask(instant).execute();
        }
    };

    private static class PopulateBdAsyTask extends AsyncTask<Void, Void, Void>{

        private NoteDio noteDio;

        public PopulateBdAsyTask(NoteDatabase bd) {
            noteDio = bd.noteDio();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDio.insert(new Note("title 1", "details 1", 1));
            noteDio.insert(new Note("title 2", "details 2", 2));
            noteDio.insert(new Note("title 3", "details 3",3));
            noteDio.insert(new Note("title 3", "details 3",4));
            return null;
        }
    }
}
